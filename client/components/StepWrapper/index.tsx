import { FC, ReactNode } from 'react';
import { Card, Row, Steps } from 'antd';

const { Step } = Steps;

interface StepWrapperProps {
  activeStep: number;
  children: ReactNode;
}

const steps = ['Track information', 'Upload image', 'Upload audio'];

const StepWrapper: FC<StepWrapperProps> = ({ activeStep, children }) => {
  return (
    <Row>
      <Steps current={activeStep}>
        {steps.map((step, index) => (
          <Step key={index} title={step} />
        ))}
      </Steps>
      <Row>
        <Card>{children}</Card>
      </Row>
    </Row>
  );
};

export default StepWrapper;
