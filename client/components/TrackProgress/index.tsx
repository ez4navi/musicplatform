import React, { FC } from 'react';
import { Col, Slider } from 'antd';
import { formatToMinutes } from '../../utils/formatToMinutes';

interface TrackProgressProps {
  left: number;
  right: number;
  onChange: (e) => void;
}

const TrackProgress: FC<TrackProgressProps> = ({ left, right, onChange }) => {
  return (
    <>
      <Col flex={'1 1 500px'} span={16} offset={1}>
        <Slider
          min={0}
          max={right}
          value={left}
          onChange={onChange}
          tipFormatter={(value) => formatToMinutes(value)}
        />
      </Col>
      <Col flex={'0 1 70px'}>
        {formatToMinutes(left)} / {formatToMinutes(right)}
      </Col>
    </>
  );
};

export default TrackProgress;
