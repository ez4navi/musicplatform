import TrackList from './TrackList';
import { ITrack } from '../../types/track';

const TrackListContainer = () => {
  const items: ITrack[] = [
    {
      id: 1,
      name: 'Track 1',
      artist: 'Aboba',
      text: 'Lorem1',
      listens: 10,
      picture: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png',
      audio:
        'http://localhost:8000/audio/58b175b7-3a79-4f8b-800c-fd829d5ecdb2.mp3',
      comments: [],
    },
    {
      id: 2,
      name: 'Track 2',
      artist: 'Aboba',
      text: 'Lorem1',
      listens: 12,
      picture: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png',
      audio:
        'http://localhost:8000/audio/58b175b7-3a79-4f8b-800c-fd829d5ecdb2.mp3',
      comments: [],
    },
    {
      id: 3,
      name: 'Track 3',
      artist: 'Aboba',
      text: 'Lorem1',
      listens: 12,
      picture: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png',
      audio:
        'http://localhost:8000/audio/58b175b7-3a79-4f8b-800c-fd829d5ecdb2.mp3',
      comments: [],
    },
    {
      id: 4,
      name: 'Track 4',
      artist: 'Aboba',
      text: 'Lorem1',
      listens: 12,
      picture: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png',
      audio:
        'http://localhost:8000/audio/58b175b7-3a79-4f8b-800c-fd829d5ecdb2.mp3',
      comments: [],
    },
    {
      id: 5,
      name: 'Track 5',
      artist: 'Aboba',
      text: 'Lorem1',
      listens: 12,
      picture: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png',
      audio:
        'http://localhost:8000/audio/58b175b7-3a79-4f8b-800c-fd829d5ecdb2.mp3',
      comments: [],
    },
    {
      id: 6,
      name: 'Track 6',
      artist: 'Aboba',
      text: 'Lorem1',
      listens: 12,
      picture: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png',
      audio:
        'http://localhost:8000/audio/58b175b7-3a79-4f8b-800c-fd829d5ecdb2.mp3',
      comments: [],
    },
    {
      id: 7,
      name: 'Track 7',
      artist: 'Aboba',
      text: 'Lorem1',
      listens: 12,
      picture: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png',
      audio:
        'http://localhost:8000/audio/58b175b7-3a79-4f8b-800c-fd829d5ecdb2.mp3',
      comments: [],
    },
    {
      id: 8,
      name: 'Track 8',
      artist: 'Aboba',
      text: 'Lorem1',
      listens: 12,
      picture: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png',
      audio:
        'http://localhost:8000/audio/58b175b7-3a79-4f8b-800c-fd829d5ecdb2.mp3',
      comments: [],
    },
  ];
  return <TrackList items={items} />;
};

export default TrackListContainer;
