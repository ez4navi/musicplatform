import { FC } from 'react';
import { Row } from 'antd';
import TrackCard from '../TrackCard';
import { ITrack } from '../../types/track';

interface TrackListProps {
  items: ITrack[];
}

const TrackList: FC<TrackListProps> = ({ items }) => {
  return (
    <Row justify={'start'} gutter={[48, 16]}>
      {items.map((item, index) => (
        <TrackCard key={index} track={item} />
      ))}
    </Row>
  );
};

export default TrackList;
