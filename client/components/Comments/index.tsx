import { FC } from 'react';
import { IComment } from '../../types/track';
import { Card, Empty, Typography } from 'antd';

interface CommentsProps {
  items: IComment[];
}

const Comments: FC<CommentsProps> = ({ items }) => {
  return (
    <div>
      {items.length > 0 ? (
        items.map((item) => (
          <Card title={item.username} key={item.id}>
            <Typography.Paragraph>{item.text}</Typography.Paragraph>
          </Card>
        ))
      ) : (
        <Empty description={'No comments'} />
      )}
    </div>
  );
};

export default Comments;
