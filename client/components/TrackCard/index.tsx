import { FC } from 'react';
import { Card, Col } from 'antd';
import { ITrack } from '../../types/track';
import {
  PauseOutlined,
  CaretRightOutlined,
  InfoOutlined,
} from '@ant-design/icons';
import { useRouter } from 'next/router';
import { useActions } from '../../hooks/useActions';
import { useTypedSelector } from '../../hooks/useTypedSelector';
const { Meta } = Card;

interface TrackItemProps {
  track: ITrack;
}

const TrackCard: FC<TrackItemProps> = ({ track }) => {
  const router = useRouter();

  const { playTrack, pauseTrack, setActiveTrack } = useActions();
  const { active, pause } = useTypedSelector((state) => state.player);

  const play = (e) => {
    e.stopPropagation();
    setActiveTrack(track);
  };

  const isActive = track.id === active?.id && !pause;

  return (
    <Col span={4}>
      <Card
        hoverable
        onClick={() => router.push(`/tracks/${track.id}`)}
        cover={<img alt="example" src={track.picture} />}
        actions={[
          isActive ? (
            <PauseOutlined onClick={play} key={'pause'} />
          ) : (
            <CaretRightOutlined onClick={play} key={'play'} />
          ),
          <InfoOutlined key={'info'} />,
        ]}
      >
        <Meta title={track.name} description={track.text} />
      </Card>
    </Col>
  );
};

export default TrackCard;
