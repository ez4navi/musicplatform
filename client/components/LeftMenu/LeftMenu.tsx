import React from 'react'
import { Layout, Menu } from 'antd'

const { Sider } = Layout

const LeftMenu = ({ isCollapsed, onCollapse, items }) => {
  return (
    <Sider collapsible collapsed={isCollapsed} onCollapse={onCollapse}>
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={['0']} mode="inline" items={items} />
    </Sider>
  )
}

export default LeftMenu
