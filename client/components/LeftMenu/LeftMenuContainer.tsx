import React, { useState } from 'react'
import LeftMenu from './LeftMenu'
import { DesktopOutlined, FileOutlined, PieChartOutlined } from '@ant-design/icons'
import { MenuProps } from 'antd'
import { useRouter } from 'next/router'

const LeftMenuContainer = () => {
  type MenuItem = Required<MenuProps>['items'][number]

  const [isCollapsed, setCollapse] = useState(false)
  const router = useRouter()

  const onCollapse = (collapsed: boolean) => {
    setCollapse(collapsed)
  }

  const getItem = (
    label: React.ReactNode,
    key: React.Key,
    href: string,
    icon?: React.ReactNode,
    children?: MenuItem[],
  ): MenuItem => {
    return {
      key,
      icon,
      children,
      label: <a onClick={() => router.push(href)}>{label}</a>,
    } as MenuItem
  }

  const items: MenuItem[] = [
    getItem('Main page', '0', '/', <FileOutlined />),
    getItem('Tracks list', '1', '/tracks', <PieChartOutlined />),
    getItem('Albums list', '2', '/albums', <DesktopOutlined />),
  ]

  return <LeftMenu isCollapsed={isCollapsed} onCollapse={onCollapse} items={items} />
}

export default LeftMenuContainer
