import React, { FC } from 'react';
import { Button, Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

interface FileUploadProps {
  setFile: Function;
  accept: string;
}

const FileUpload: FC<FileUploadProps> = ({ setFile, accept }) => {
  const onChange = ({ fileList }) => {
    setFile(fileList[0]);
  };

  return (
    <div>
      <Upload accept={accept} onChange={onChange} maxCount={1}>
        <Button icon={<UploadOutlined />}>Click to Upload</Button>
      </Upload>
    </div>
  );
};

export default FileUpload;
