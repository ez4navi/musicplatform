import React from 'react'
import { Layout } from 'antd'
const { Header } = Layout
import styles from './Navbar.module.scss'

const Navbar = () => {
  return <Header className={styles.headerBackground} style={{ padding: 0 }} />
}

export default Navbar
