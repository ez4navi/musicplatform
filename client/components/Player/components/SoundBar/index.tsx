import React, { FC } from 'react';
import { Col, Popover, Slider } from 'antd';
import styles from './SoundBar.module.scss';
import { SoundOutlined } from '@ant-design/icons';

interface SoundBarProps {
  volume: number;
  onChange: (e: number) => void;
}

const SoundBar: FC<SoundBarProps> = ({ onChange, volume }) => {
  const content = (
    <div className={styles.wrapper}>
      <Slider
        autoFocus
        vertical
        min={0}
        max={100}
        value={volume}
        onChange={onChange}
        tooltipPlacement={'left'}
      />
    </div>
  );
  return (
    <Popover content={content}>
      <Col className={styles.sound}>
        <SoundOutlined />
      </Col>
    </Popover>
  );
};

export default SoundBar;
