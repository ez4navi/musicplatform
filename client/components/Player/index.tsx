import React, { useEffect } from 'react';
import { Col, Row, Typography } from 'antd';
import { CaretRightOutlined, PauseOutlined } from '@ant-design/icons';
import styles from './Player.module.scss';
import TrackProgress from '../TrackProgress';
import SoundBar from './components/SoundBar';
import classNames from 'classnames';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { useActions } from '../../hooks/useActions';

let audio;

const Player = () => {
  const { pause, volume, duration, currentTime, active } = useTypedSelector(
    (state) => state.player,
  );
  const { pauseTrack, playTrack, setVolume, setCurrentTime, setDuration } =
    useActions();

  useEffect(() => {
    if (!audio) {
      audio = new Audio();
    } else {
      setAudio();
      playTrack();
      audio.play();
    }
  }, [active]);

  const setAudio = () => {
    if (active) {
      audio.src = active.audio;
      audio.volume = volume / 100;
      audio.onloadedmetadata = () => {
        setDuration(Math.ceil(audio.duration));
      };
      audio.ontimeupdate = () => {
        setCurrentTime(Math.ceil(audio.currentTime));
      };
    }
  };

  const play = () => {
    if (pause) {
      playTrack();
      audio.play();
    } else {
      pauseTrack();
      audio.pause();
    }
  };

  const changeVolume = (e: number) => {
    audio.volume = e / 100;
    setVolume(e);
  };

  const changeCurrentTime = (e: number) => {
    audio.currentTime = e;
    setCurrentTime(e);
  };

  if (!active) {
    return null;
  }

  return (
    <Row className={styles.player}>
      <Col className={styles.player__item} onClick={play}>
        {pause ? <CaretRightOutlined /> : <PauseOutlined />}
      </Col>
      <Col
        className={classNames(styles.player__item, styles.player__trackName)}
      >
        <Row>
          <Typography.Text>Track name - {active?.name}</Typography.Text>
        </Row>
        <Row>
          <Typography.Text>Artist - {active?.artist}</Typography.Text>
        </Row>
      </Col>
      <TrackProgress
        left={currentTime}
        right={duration}
        onChange={changeCurrentTime}
      />
      <SoundBar onChange={changeVolume} volume={volume} />
    </Row>
  );
};

export default Player;
