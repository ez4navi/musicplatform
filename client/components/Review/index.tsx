import React from 'react';
import { Button, Input, Row, Typography } from 'antd';
import styles from './Review.module.scss';
const { TextArea } = Input;

const Review = () => {
  return (
    <Row>
      <Typography.Title level={3}>Reviews</Typography.Title>
      <Input className={styles.name} placeholder="Your name" allowClear />
      <TextArea className={styles.review} placeholder="Review" allowClear />
      <Button className={styles.button}>Submit</Button>
    </Row>
  );
};

export default Review;
