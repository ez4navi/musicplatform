import { FC, useState } from 'react';
import StepWrapper from '../../components/StepWrapper';
import { Button, Row } from 'antd';
import FileUpload from '../../components/FileUpload';

const Create: FC = () => {
  const [activeStep, setActiveStep] = useState(0);
  const [picture, setPicture] = useState(null);
  const [audio, setAudio] = useState(null);
  const next = () => {
    if (activeStep !== 2) {
      setActiveStep((prev) => prev + 1);
    }
  };

  const back = () => {
    setActiveStep((prev) => prev - 1);
  };

  return (
    <div>
      <StepWrapper activeStep={activeStep}>
        {activeStep === 1 && (
          <FileUpload accept={'image/*'} setFile={setPicture} />
        )}
        {activeStep === 2 && (
          <FileUpload accept={'audio/*'} setFile={setAudio} />
        )}
      </StepWrapper>
      <Row justify={'space-between'}>
        <Button disabled={activeStep === 0} onClick={back}>
          Back
        </Button>
        <Button onClick={next}>Next</Button>
      </Row>
    </div>
  );
};

export default Create;
