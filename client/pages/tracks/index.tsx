import { FC } from 'react';
import { Button, Col, Row, Typography } from 'antd';
import TrackList from '../../components/TrackList';
import styles from './Tracks.module.scss';
import { UploadOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router';
import { useTypedSelector } from '../../hooks/useTypedSelector';

const Tracks: FC = () => {
  const router = useRouter();
  const {} = useTypedSelector((state) => state.player);
  return (
    <>
      <Row justify={'space-between'}>
        <Col span={8} offset={8}>
          <Typography.Title level={1} className={styles.title}>
            Track list
          </Typography.Title>
        </Col>
        <Col>
          <Button
            onClick={() => router.push('/tracks/create')}
            type="primary"
            icon={<UploadOutlined />}
          />
        </Col>
      </Row>
      <Row>
        <TrackList />
      </Row>
    </>
  );
};

export default Tracks;
