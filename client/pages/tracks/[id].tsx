import React from 'react';
import { ITrack } from '../../types/track';
import { Col, Image, Row, Typography } from 'antd';
import Review from '../../components/Review';
import Comments from '../../components/Comments';

const { Paragraph } = Typography;

const TrackPage = () => {
  const track: ITrack = {
    id: 1,
    name: 'Track 1',
    artist: 'Aboba',
    text: 'Lorem1',
    listens: 10,
    picture: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png',
    audio: 'qwe',
    comments: [],
  };
  return (
    <div>
      <Row gutter={[16, 0]}>
        <Col span={8}>
          <Image src={track.picture} alt={'track picture'} />
        </Col>
        <Col span={16}>
          <Row>
            <Typography.Title level={2}>
              Artist - {track.artist}
            </Typography.Title>
          </Row>
          <Row>
            <Typography.Title level={1}>
              Track name - {track.name}
            </Typography.Title>
          </Row>
          <Row>
            <Paragraph>Listens: {track.listens}</Paragraph>
          </Row>
        </Col>
      </Row>
      <Row>
        <Paragraph>Lyrics</Paragraph>
      </Row>
      <Row>
        <Paragraph>{track.text}</Paragraph>
      </Row>
      <Review />
      <Comments items={track.comments} />
    </div>
  );
};

export default TrackPage;
