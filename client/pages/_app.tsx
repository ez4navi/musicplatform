import '../styles/globals.scss';
import 'antd/dist/antd.css';
import AppLayout from '../layouts/App';

import { FC } from 'react';
import { AppProps } from 'next/app';
import { wrapper } from '../store';

const WrappedApp: FC<AppProps> = ({ Component, pageProps }) => (
  <AppLayout>
    <Component {...pageProps} />
  </AppLayout>
);

export default wrapper.withRedux(WrappedApp);
