import React, { FC } from 'react'

const App: FC = () => {
  return (
    <div className={'center'}>
      <h1>Добро пожаловать!</h1>
      <h3>Здесь собраны лучшие треки!</h3>
    </div>
  )
}

export default App
