import { FunctionComponent, ReactNode } from 'react';
import { Layout } from 'antd';
import Navbar from '../../components/Navbar';
import LeftMenu from '../../components/LeftMenu';
import Player from '../../components/Player';

const { Content } = Layout;

interface BaseLayoutProps {
  children?: ReactNode;
}

const AppLayout: FunctionComponent<BaseLayoutProps> = ({ children }) => {
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <LeftMenu />
      <Layout className="site-layout">
        <Navbar />
        <Content
          style={{
            margin: '0 auto',
            padding: '0 16px',
            width: '100%',
            maxWidth: '1920px',
          }}
        >
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 360 }}
          >
            {children}
          </div>
          <Player />
        </Content>
      </Layout>
    </Layout>
  );
};

export default AppLayout;
