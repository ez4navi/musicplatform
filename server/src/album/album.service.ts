import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Album } from '../models/album.model';
import { Track } from '../models/tracks.model';
import { CreateAlbumDto } from './dto/create-album.dto';

@Injectable()
export class AlbumService {
  constructor(
    @InjectModel(Album) private albumModel: typeof Album,
    @InjectModel(Track) private trackModel: typeof Track,
  ) {}

  async create(dto: CreateAlbumDto): Promise<Album> {
    return await this.albumModel.create(dto);
  }

  async getAll() {
    return await this.albumModel.findAll();
  }

  async getOne(id: number): Promise<Album> {
    return await this.albumModel.findByPk(id, { include: { all: true } });
  }

  async delete(id: number) {
    return await this.albumModel.destroy({ where: { id } });
  }

  async addTrackToAlbum({
    id,
    track_id,
  }: {
    id: number;
    track_id: number;
  }): Promise<Track> {
    const track = await this.trackModel.findByPk(track_id);
    track.album_id = id;
    await track.save();
    return track;
  }
}
