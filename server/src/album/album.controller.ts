import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { AlbumService } from './album.service';
import { CreateAlbumDto } from './dto/create-album.dto';
import { Album } from '../models/album.model';
import { AddTrackToAlbumDto } from './dto/add-track-to-album.dto';
import { Track } from '../models/tracks.model';

@Controller('/albums')
export class AlbumController {
  constructor(private albumService: AlbumService) {}

  @Post()
  create(@Body() dto: CreateAlbumDto): Promise<Album> {
    return this.albumService.create(dto);
  }

  @Get()
  getAll() {
    return this.albumService.getAll();
  }

  @Get(':id')
  getOne(@Param('id') id: number): Promise<Album> {
    return this.albumService.getOne(id);
  }

  @Delete(':id')
  delete(@Param('id') id: number) {
    return this.albumService.delete(id);
  }

  @Post(':id')
  addTrackToAlbum(
    @Body() dto: AddTrackToAlbumDto,
    @Param('id') id: number,
  ): Promise<Track> {
    return this.albumService.addTrackToAlbum({ track_id: dto.track_id, id });
  }
}
