import { Module } from '@nestjs/common';
import { AlbumService } from './album.service';
import { AlbumController } from './album.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Album } from '../models/album.model';
import { Track } from '../models/tracks.model';

@Module({
  providers: [AlbumService],
  controllers: [AlbumController],
  imports: [SequelizeModule.forFeature([Album, Track])],
})
export class AlbumModule {}
