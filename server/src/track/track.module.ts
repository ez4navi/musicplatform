import { Module } from '@nestjs/common';
import { TrackService } from './track.service';
import { TrackController } from './track.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Track } from '../models/tracks.model';
import { Comment } from '../models/comment.model';
import { FileService } from '../file/file.service';

@Module({
  providers: [TrackService, FileService],
  controllers: [TrackController],
  imports: [SequelizeModule.forFeature([Track, Comment])],
})
export class TrackModule {}
