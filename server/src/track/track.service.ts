import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Track } from '../models/tracks.model';
import { Comment } from '../models/comment.model';
import { CreateTrackDto } from './dto/create-track.dto';
import { CreateCommentDto } from './dto/create-comment.dto';
import { FileService, FileType } from '../file/file.service';
import { Op } from 'sequelize';

@Injectable()
export class TrackService {
  constructor(
    @InjectModel(Track) private trackModel: typeof Track,
    @InjectModel(Comment) private commentModel: typeof Comment,
    private fileService: FileService,
  ) {}

  async create(dto: CreateTrackDto, picture, audio): Promise<Track> {
    const audioPath = this.fileService.createFile(FileType.AUDIO, audio);
    const picturePath = this.fileService.createFile(FileType.IMAGE, picture);

    const track = await this.trackModel.create({
      ...dto,
      listens: 0,
      audio: audioPath,
      picture: picturePath,
    });
    return track;
  }

  async getAll(count = 10, offset = 0): Promise<Track[]> {
    const tracks = await this.trackModel.findAll({
      limit: count,
      offset,
      include: {
        model: Comment,
        attributes: ['id'],
      },
    });

    return tracks;
  }

  async getOne(id: number): Promise<Track> {
    return await this.trackModel.findByPk(id, { include: { all: true } });
  }

  async delete(id: number) {
    return await this.trackModel.destroy({ where: { id } });
  }

  async createComment(dto: CreateCommentDto): Promise<Comment> {
    return await this.commentModel.create(dto);
  }

  async listen(id: number) {
    const track = await this.trackModel.findByPk(id);
    track.listens += 1;
    await track.save();
  }

  async search(query = ''): Promise<Track[]> {
    const tracks = await this.trackModel.findAll({
      where: {
        name: {
          [Op.iLike]: `%${query}%`,
        },
      },
    });
    return tracks;
  }
}
