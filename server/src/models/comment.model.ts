import {BelongsTo, Column, DataType, ForeignKey, Model, Table} from "sequelize-typescript";
import {Track} from "./tracks.model";

interface CommentCreationAttrs {
    username: string;
    text: string;
    track_id: number;
}

@Table({tableName: 'comments'})
export class Comment extends Model<Comment, CommentCreationAttrs> {
    @Column({type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true})
    id: number;

    @Column({type: DataType.STRING})
    username: string;

    @Column({type: DataType.STRING})
    text: string;

    @ForeignKey(() => Track)
    @Column({type: DataType.INTEGER})
    track_id: number;

    @BelongsTo(() => Track)
    track: Track
}
