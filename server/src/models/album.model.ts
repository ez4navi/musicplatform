import {Column, DataType, HasMany, Model, Table} from "sequelize-typescript";
import {Track} from "./tracks.model";

interface AlbumCreationAttrs {
    name: string;
    artist: string;
}

@Table({tableName: 'albums'})
export class Album extends Model<Album, AlbumCreationAttrs> {
    @Column({type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true})
    id: number;

    @Column({type: DataType.STRING})
    name: string;

    @Column({type: DataType.STRING})
    artist: string;

    @Column({type: DataType.STRING})
    picture: string;

    @HasMany(() => Track)
    tracks: Track[];
}
