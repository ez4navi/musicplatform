import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  HasMany,
  Model,
  Table,
  DefaultScope,
} from 'sequelize-typescript';
import { Comment } from './comment.model';
import { Album } from './album.model';

interface TrackCreationAttrs {
  name: string;
  artist: string;
  text: string;
  audio: string;
  picture: string;
  listens: number;
}

@Table({ tableName: 'tracks' })
@DefaultScope({
  order: [['createdAt', 'ASC']],
})
export class Track extends Model<Track, TrackCreationAttrs> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({ type: DataType.STRING })
  name: string;

  @Column({ type: DataType.STRING })
  artist: string;

  @Column({ type: DataType.STRING })
  text: string;

  @Column({ type: DataType.INTEGER })
  listens: number;

  @Column({ type: DataType.STRING })
  picture: string;

  @Column({ type: DataType.STRING })
  audio: string;

  @ForeignKey(() => Album)
  @Column({ type: DataType.INTEGER })
  album_id: number;

  @HasMany(() => Comment)
  comments: Comment[];

  @BelongsTo(() => Album)
  album: Album;
}
